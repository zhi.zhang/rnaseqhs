rnaseqhs package
================

Subpackages
-----------

.. toctree::

    rnaseqhs.tests

Submodules
----------

rnaseqhs.Q20_1 module
---------------------

.. automodule:: rnaseqhs.Q20_1
    :members:
    :undoc-members:
    :show-inheritance:

rnaseqhs.command_line module
----------------------------

.. automodule:: rnaseqhs.command_line
    :members:
    :undoc-members:
    :show-inheritance:

rnaseqhs.rmN module
-------------------

.. automodule:: rnaseqhs.rmN
    :members:
    :undoc-members:
    :show-inheritance:

rnaseqhs.rnaseqhs module
------------------------

.. automodule:: rnaseqhs.rnaseqhs
    :members:
    :undoc-members:
    :show-inheritance:

rnaseqhs.sortCover2 module
--------------------------

.. automodule:: rnaseqhs.sortCover2
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: rnaseqhs
    :members:
    :undoc-members:
    :show-inheritance:
